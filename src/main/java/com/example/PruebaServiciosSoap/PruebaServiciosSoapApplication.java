package com.example.PruebaServiciosSoap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaServiciosSoapApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaServiciosSoapApplication.class, args);
	}

}
